BHT OAUTH API

===================================
OAuth Consumer Registration
===================================
POST
http://{HOSTNAME}:8080/bht-vbox/cxf/oauth/registerConsumer

BODY
appName={APPNAME}
appDescription={APPDESCRIPTION}


primjer:
http://localhost:8080/bht-vbox/cxf/oauth/registerConsumer


===================================
OAuth AccessToken Request
===================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/oauth/token

HEADER
Authorization=Basic Base64Encode{clientID:clientSecret}

BODY
username={APPUSERNAME}
password={APPPASSWORD}
grant_type=password


primjer
http://localhost:8080/bht-vbox/cxf/oauth/token

NAPOMENA: Koristimo ResourceOwner grant type (određuje OAuth flow)


===================================
ACTIVE - Provjera aktivacija VBOX
===================================
GET
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/active

HEADER
Authorization={Bearer ACCESSTOKEN}

primjer:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/active

===================================
SENDSMS - Salje SMS
===================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/sendSMS/{destinationnumber}

HEADER
Authorization={Bearer ACCESSTOKEN}

BODY
text={textmessage}

primjer:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/sendSMS/38761139310


=========================================
LIST INBOX - Lista INBOX poruke iz opsega
=========================================

GET
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/inbox/{startSMS}/{endSMS}

HEADER
Authorization={Bearer ACCESSTOKEN}

primjer 1:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/inbox/1/1
Dohvaca prvu poruku iz inboxa

primjer 2:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/inbox/1/5
Dohvaca prvih pet poruka iz inboxa


=========================================
LIST SENT - Lista SENT poruke iz opsega
=========================================
GET
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/sent/{startSMS}/{endSMS}

HEADER
Authorization={Bearer ACCESSTOKEN}


primjer:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/sent/1/1
Dohvaca prvu poruku iz sent foldera

primjer 2:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/sent/1/5
Dohvaca prvih pet poruka iz sent foldera


==========================================
LIST DRAFTS - Lista DRAFT poruke iz opsega
==========================================

GET
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/drafts

HEADER
Authorization={Bearer ACCESSTOKEN}

primjer:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/drafts
Dohvaca sve poruke iz draft foldera

==========================================
SAVE AS DRAFT - Čuva poruku u DRAFT folder
==========================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/saveSMS/{destinationnumber}

HEADER
Authorization={Bearer ACCESSTOKEN}

BODY
text={textmessage}


primjer:
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/saveSMS/38761139310

==============================================================
SEND DRAFT - Šalje poruku prethodno sačuvanu u DRAFT folder
==============================================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/drafts/send/{ID}

HEADER
Authorization={Bearer ACCESSTOKEN}

BODY
messageID={DRAFT_ID}

primjer
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/drafts/send/641


==============================================================
DELETE DRAFT - Briše poruku prethodno sačuvanu u DRAFT folder
==============================================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/drafts/delete/{ID}

HEADER
Authorization={Bearer ACCESSTOKEN}

primjer
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/223553/drafts/delete/641


==============================================================
MARK AS READ - Označi poruku kao pročitanu u Inbox-u
==============================================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/inbox/{id}/read

HEADER
Authorization={Bearer ACCESSTOKEN}


primjer
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/610639/inbox/64016057/read


==============================================================
MARK AS UNREAD - Označi poruku kao nepročitanu u Inbox-u
==============================================================

POST
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/inbox/{id}/unread

HEADER
Authorization={Bearer ACCESSTOKEN}


primjer
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/610639/inbox/64016057/unread



==============================================================
DELETE INBOX MESSAGE - Obriši poruku u INBOX folderu
==============================================================

DELETE
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/inbox/{id}/delete

HEADER
Authorization={Bearer ACCESSTOKEN}


primjer
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/610639/inbox/63021298/delete



==============================================================
DELETE SENT MESSAGE - Obriši poruku u SENT folderu
==============================================================

DELETE
http://{HOSTNAME}:8080/bht-vbox/cxf/rest-api/fsmsc/{areacode}/{number}/sent/{id}/delete

HEADER
Authorization={Bearer ACCESSTOKEN}

primjer
http://localhost:8080/bht-vbox/cxf/rest-api/fsmsc/033/610639/sent/64451479/delete


==============================================================
CUSTOM LDAP AUTH - Autorizacija korisnika na LDAP
==============================================================

GET
http://{HOSNAME}:8080/bht-vbox/cxf/custom-api/customauth/direct

HEADER
Authorization=Basic base64Enc{username@domain:password}