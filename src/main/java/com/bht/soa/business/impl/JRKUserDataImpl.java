package com.bht.soa.business.impl;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bht.soa.business.JRKUserData;
import com.bht.soa.business.exception.BANotFoundException;
import com.bht.soa.business.exception.CaNotFoundException;
import com.bht.soa.business.exception.PDFNotFoundException;
import com.bht.soa.gen.jrk.auth.GetsabacaBeansElement;
import com.bht.soa.gen.jrk.auth.GetsabacaBeansResponseElement;
import com.bht.soa.gen.jrk.auth.GetsabacaRowUser;
import com.bht.soa.gen.jrk.auth.PORTALAUTHWS;
import com.bht.soa.gen.jrk.auth.PORTALAUTHWS_Service;
import com.bht.soa.gen.jrk.bainfo.CustomerInfo;
import com.bht.soa.gen.jrk.bainfo.EventSource;
import com.bht.soa.gen.jrk.bainfo.GetCustomerBAInfoElement;
import com.bht.soa.gen.jrk.bainfo.GetCustomerBAInfoResponseElement;
import com.bht.soa.gen.jrk.bainfo.PortalServices;
import com.bht.soa.gen.jrk.bainfo.PortalServices_Service;
import com.bht.soa.gen.jrk.pdfurl.KopijaRac;
import com.bht.soa.gen.jrk.pdfurl.KopijaRac_Service;
import com.bht.soa.gen.jrk.pdfurl.LinkKopijaRacElement;
import com.bht.soa.gen.jrk.pdfurl.LinkKopijaRacResponseElement;

public class JRKUserDataImpl implements JRKUserData {


	private static final String JRKAuthWSDL_WS_PROPERTY = "ws.portalauthws.service";
	private static final String JRKBaInfoWSDL_WS_PROPERTY = "ws.portalservices.service";
	private static final String JRKPDFBillWSDL_WS_PROPERTY = "ws.kopijarac.service";

	/* WSDL endpoint for PORTALAUTHWS_Service */
	private String JRKAuthWSDL = "";

	/* WSDL endpoint for PortalServices_Service */
	private String JRKBaInfoWSDL = "";

	/* WSDL endpoint for KopijaRac_Service */
	private String JRKPDFBillWSDL = "";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public JRKUserDataImpl() {

	}

	public JRKUserDataImpl(HashMap<String, String> serviceProperties) {
		this.JRKAuthWSDL = serviceProperties.get(JRKAuthWSDL_WS_PROPERTY);
		this.JRKBaInfoWSDL = serviceProperties.get(JRKBaInfoWSDL_WS_PROPERTY);
		this.JRKPDFBillWSDL = serviceProperties.get(JRKPDFBillWSDL_WS_PROPERTY);
	}

	@Override
	public BigDecimal getUserCA(String areaCode, BigDecimal number) throws CaNotFoundException {
		// URL wsdlURL = PORTALAUTHWS_Service.WSDL_LOCATION;
		URL wsdlURL;
		try {
			wsdlURL = new URL(JRKAuthWSDL);
		} catch (MalformedURLException e) {
			log.error("Unable to construct WebService call. Invalid WSDL URL");
			throw new RuntimeException();
		}
		PORTALAUTHWS_Service ss = new PORTALAUTHWS_Service(wsdlURL);
		PORTALAUTHWS port = ss.getPORTALAUTHWSSoapHttpPort();

		GetsabacaBeansElement saBaCaBean = new GetsabacaBeansElement();
		saBaCaBean.setPAreaCode(areaCode);
		saBaCaBean.setPMsisdn(number);

		GetsabacaBeansResponseElement response = port.getsabacaBeans(saBaCaBean);

		List<GetsabacaRowUser> userList = response.getResult();
		if (userList == null || userList.size() == 0) {
			throw new CaNotFoundException();
		}

		GetsabacaRowUser userData = userList.get(0);
		return userData.getCcaaid();
	}

	@Override
	public BigDecimal getUserCA(String username) throws CaNotFoundException {

		// URL wsdlURL = PORTALAUTHWS_Service.WSDL_LOCATION;
		URL wsdlURL;
		try {
			wsdlURL = new URL(JRKAuthWSDL);
		} catch (MalformedURLException e) {
			log.error("Unable to construct WebService call. Invalid WSDL URL");
			throw new RuntimeException();
		}
		PORTALAUTHWS_Service ss = new PORTALAUTHWS_Service(wsdlURL);
		PORTALAUTHWS port = ss.getPORTALAUTHWSSoapHttpPort();

		GetsabacaBeansElement saBaCaBean = new GetsabacaBeansElement();
		saBaCaBean.setPUsername(username);

		GetsabacaBeansResponseElement response = port.getsabacaBeans(saBaCaBean);

		List<GetsabacaRowUser> userList = response.getResult();
		if (userList == null || userList.size() == 0) {
			throw new CaNotFoundException();
		}

		GetsabacaRowUser userData = userList.get(0);
		return userData.getCcaaid();
	}

	@Override
	public List<EventSource> getEventSources(String username, String techCode, String className) throws BANotFoundException {

		BigDecimal caId;
		try {
			caId = this.getUserCA(username);
		} catch (CaNotFoundException e) {
			throw new BANotFoundException("Couldn't lookup BA Info because CA ID was not found!");
		}

		//URL wsdlURL = PortalServices_Service.WSDL_LOCATION;
		URL wsdlURL;
		try {
			wsdlURL = new URL(JRKBaInfoWSDL);
		} catch (MalformedURLException e) {
			log.error("Unable to construct WebService call. Invalid WSDL URL");
			throw new RuntimeException();
		}
		
		PortalServices_Service ss = new PortalServices_Service(wsdlURL);
		PortalServices port = ss.getPortalServicesSoapHttpPort();

		// WS params
		GetCustomerBAInfoElement baInfoParams = new GetCustomerBAInfoElement();
		baInfoParams.setCaId(caId);

		GetCustomerBAInfoResponseElement response = port.getCustomerBAInfo(baInfoParams);

		if (response.getResult() == null) {
			throw new BANotFoundException();
		} else if (response.getResult().getBa() == null || response.getResult().getBa().isEmpty()) {
			throw new BANotFoundException();
		}

		List<CustomerInfo> baList = response.getResult().getBa();

		List<EventSource> returnList = new ArrayList<EventSource>();

		for (CustomerInfo customerInfo : baList) {
			if (customerInfo.getEventsource() == null || customerInfo.getEventsource().isEmpty()) {
				continue;
			} else {
				if (customerInfo.getTechnologyCode().equals(techCode)) {
					for (EventSource es : customerInfo.getEventsource()) {
						if (es.getClassName() != null && "" != es.getClassName() && es.getClassName().equals(className)) {
							returnList.add(es);
						}
					}
				}

			}
		}

		return returnList;
	}

	@Override
	public List<EventSource> getEventSources(String areaCode, BigDecimal number, String techCode, String className) throws BANotFoundException {

		BigDecimal caId;
		try {
			caId = this.getUserCA(areaCode, number);
		} catch (CaNotFoundException e) {
			throw new BANotFoundException("Couldn't lookup BA Info because CA ID was not found!");
		}

		//URL wsdlURL = PortalServices_Service.WSDL_LOCATION;
		URL wsdlURL;
		try {
			wsdlURL = new URL(JRKBaInfoWSDL);
		} catch (MalformedURLException e) {
			log.error("Unable to construct WebService call. Invalid WSDL URL");
			throw new RuntimeException();
		}
		PortalServices_Service ss = new PortalServices_Service(wsdlURL);
		PortalServices port = ss.getPortalServicesSoapHttpPort();

		// WS params
		GetCustomerBAInfoElement baInfoParams = new GetCustomerBAInfoElement();
		baInfoParams.setCaId(caId);

		GetCustomerBAInfoResponseElement response = port.getCustomerBAInfo(baInfoParams);

		if (response.getResult() == null) {
			throw new BANotFoundException();
		} else if (response.getResult().getBa() == null || response.getResult().getBa().isEmpty()) {
			throw new BANotFoundException();
		}

		List<CustomerInfo> baList = response.getResult().getBa();

		List<EventSource> returnList = new ArrayList<EventSource>();

		for (CustomerInfo customerInfo : baList) {
			if (customerInfo.getEventsource() == null || customerInfo.getEventsource().isEmpty()) {
				continue;
			} else {
				if (customerInfo.getTechnologyCode().equals(techCode)) {
					for (EventSource es : customerInfo.getEventsource()) {
						if (es.getClassName() != null && "" != es.getClassName() && es.getClassName().equals(className)) {
							returnList.add(es);
						}
					}
				}

			}
		}

		return returnList;

	}

	@Override
	public List<EventSource> getEventSources(String areaCode, BigDecimal number, String[] techCodes, String[] classNames) throws BANotFoundException {

		BigDecimal caId;
		try {
			caId = this.getUserCA(areaCode, number);
		} catch (CaNotFoundException e) {
			throw new BANotFoundException("Couldn't lookup BA Info because CA ID was not found!");
		}

		//URL wsdlURL = PortalServices_Service.WSDL_LOCATION;
		URL wsdlURL;
		try {
			wsdlURL = new URL(JRKBaInfoWSDL);
		} catch (MalformedURLException e) {
			log.error("Unable to construct WebService call. Invalid WSDL URL");
			throw new RuntimeException();
		}
		PortalServices_Service ss = new PortalServices_Service(wsdlURL);
		PortalServices port = ss.getPortalServicesSoapHttpPort();

		// WS params
		GetCustomerBAInfoElement baInfoParams = new GetCustomerBAInfoElement();
		baInfoParams.setCaId(caId);

		GetCustomerBAInfoResponseElement response = port.getCustomerBAInfo(baInfoParams);

		if (response.getResult() == null) {
			throw new BANotFoundException();
		} else if (response.getResult().getBa() == null || response.getResult().getBa().isEmpty()) {
			throw new BANotFoundException();
		}

		List<CustomerInfo> baList = response.getResult().getBa();

		List<EventSource> returnList = new ArrayList<EventSource>();

		List<String> cNames = Arrays.asList(classNames);
		List<String> tCodes = Arrays.asList(techCodes);

		for (CustomerInfo customerInfo : baList) {
			if (customerInfo.getEventsource() == null || customerInfo.getEventsource().isEmpty()) {
				continue;
			} else {
				if (tCodes.contains(customerInfo.getTechnologyCode())) {
					for (EventSource es : customerInfo.getEventsource()) {
						if (es.getClassName() != null && "" != es.getClassName()) {
							for(String cName : cNames){
								if(es.getClassName().contains(cName)){
									returnList.add(es);
								}
							}
						}
					}
				}

			}
		}

		return returnList;
	}
	
	@Override
	public String getBillAuthToken(String areaCode, BigDecimal number) throws BANotFoundException, PDFNotFoundException {
		
		String[] techCodes = {"950", "100"};
		String[] classNames = {"1 - Fiksni"};
		
		List<EventSource> events = getEventSources(areaCode, number, techCodes, classNames);
		if(events != null && !events.isEmpty()){
			BigDecimal baID = events.get(0).getBacustomerId();
			
			String billID;
			URL billURL;
			try {
				billURL = new URL(JRKPDFBillWSDL);
			} catch (MalformedURLException e) {
				log.error("Unable to construct WebService call. Invalid WSDL URL");
				throw new RuntimeException();
			}
			KopijaRac_Service pdfService = new KopijaRac_Service(billURL);
			KopijaRac port = pdfService.getKopijaRacSoapHttpPort();
			
			//WS params
			LinkKopijaRacElement paramSet = new LinkKopijaRacElement();
			paramSet.setBaId1(baID);
			paramSet.setMjes(new BigDecimal(-1));
			
			//Teh 100
			paramSet.setSifTeh("100");
			LinkKopijaRacResponseElement response = port.linkKopijaRac(paramSet);
			if(response.getResult() == null || response.getResult().isEmpty() ) {
				throw new PDFNotFoundException();
			}else{
				String httpURL = response.getResult();
				int billIdBeginIndex = httpURL.indexOf("&sif_kor=") + 9;
				int billIdEndIndex = httpURL.indexOf("&DIR=");
				billID = httpURL.substring(billIdBeginIndex, billIdEndIndex);
			}
			//Teh 950
			paramSet.setSifTeh("950");
			response = port.linkKopijaRac(paramSet);
			if(response.getResult() == null || response.getResult().isEmpty() ) {
				throw new PDFNotFoundException();
			}else{
				String httpURL = response.getResult();
				int billIdBeginIndex = httpURL.indexOf("&sifra=") + 7;
				int billIdEndIndex = httpURL.indexOf("&DIRekcija");
				billID = httpURL.substring(billIdBeginIndex, billIdEndIndex);
			}
			
			return billID;
		
		}else {
			throw new PDFNotFoundException();
		}
		
	}
	
}
