package com.bht.soa.business.impl;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.bht.soa.business.VBoxBusinessService;
import com.bht.soa.business.exception.BANotFoundException;
import com.bht.soa.data.FSMSCData;
import com.bht.soa.gen.vbox.SmsObject;
import com.bht.soa.gen.vbox.WspPortType;
import com.bht.soa.gen.vbox.WspPortTypeProxy;
import com.bht.soa.gen.vbox.holders.SmsArrayHolder;

public class VBoxBusinessServiceImpl implements VBoxBusinessService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FSMSCData userHasFSMSCctivated(String areaCode, BigDecimal number)
			throws BANotFoundException {

		
		
		/**
		String[] allowedClassNames = new String[] { "VoIP-Moja TV",
				"1 - Fiksni priključak" };
		String[] allowedTechCodes = new String[] { "950", "100" };
		JRKUserData jrkService = new JRKUserDataImpl();

		List<EventSource> esList = jrkService.getEventSources(areaCode, number,
				allowedTechCodes, allowedClassNames);

		FSMSCData data;
		data = new FSMSCData();
		data.setMessage("No valid eventsource found for this user!");
		data.setActivated(Boolean.FALSE);
		data.setStatus(HttpStatus.NOT_FOUND.name());

		if (esList != null && !esList.isEmpty()) {

			for (EventSource eventSource : esList) {
				if (eventSource.getAreaCode().equals(areaCode)
						&& eventSource.getMsisdn().equals(number.toString())) {

					List<ProductOffer> mainProdOffers = eventSource
							.getMainProductoffer();
					if (null != mainProdOffers && !mainProdOffers.isEmpty()) {
						for (ProductOffer mainOffer : mainProdOffers) {
							if (mainOffer.getName().equals(
									"Aktivacija FSMSC SMS  - POTS")
									|| mainOffer.getName().equals(
											"Aktivacija FSMSC - VOIP")) {
								data = new FSMSCData();
								data.setMessage("Korisnik ima aktiviranu uslugu FSMSC!");
								data.setActivated(Boolean.TRUE);
								data.setStatus(HttpStatus.OK.name());
							} else {
								for (ProductOffer addOffer : mainOffer
										.getAdditionalProductoffer()) {
									if (addOffer.getName().equals(
											"Aktivacija FSMSC SMS  - POTS")
											|| addOffer.getName().equals(
													"Aktivacija FSMSC - VOIP")) {
										data = new FSMSCData();
										data.setMessage("Korisnik ima aktiviranu uslugu FSMSC!");
										data.setActivated(Boolean.TRUE);
										data.setStatus(HttpStatus.OK.name());
									}
								}
							}

						}

					}
				}
			}
		}
		*/
		String source = "387" + areaCode.substring(1) + number;
		WspPortType port = new WspPortTypeProxy();

		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		StringHolder cat = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			if (port != null)
				port.checkRegistration(source, code, description, cat);
			if (description.value.contains("NEUSPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.NOT_FOUND.name());
								
			} else if(description.value.contains("USPJESNO")){
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
			}

		} catch (RemoteException e) {
			logger.error("Exception when trying to check user activation: ".concat(e.getMessage()));
		}

		return data;
	}

	@Override
	public FSMSCData sendMessage(String sourcePhone, String destinationPhone,
			String text) {

		WspPortType port = new WspPortTypeProxy();

		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			if (port != null)
				port.sendMessage(sourcePhone, destinationPhone, text, code,
						description);
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setDestinationNumber(destinationPhone);
				data.setStatus(HttpStatus.OK.name());
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setDestinationNumber(destinationPhone);
				data.setStatus(HttpStatus.NOT_FOUND.name());
			}

		} catch (RemoteException e) {
			logger.error("Exception when trying to send message: ".concat(e
					.getMessage()));

		}

		return data;
	}

	@Override
	public List<SmsObject> getInboxMessageList(String sourcePhone,
			Integer startMessage, Integer endMessage) {

		WspPortType port = new WspPortTypeProxy();

		IntHolder code = new IntHolder();
		IntHolder resultCount = new IntHolder();
		IntHolder totalCount = new IntHolder();
		StringHolder description = new StringHolder();
		SmsArrayHolder messages = new SmsArrayHolder();

		try {
			if (port != null) {

				port.getMessageList(sourcePhone, "inbox", startMessage,
						endMessage, code, description, resultCount, totalCount,
						messages);
			}

		} catch (RemoteException e) {
			logger.error("Exception when trying to get INBOX messages: "
					.concat(e.getMessage()));
		}

		return (null == messages.value) ? new ArrayList<SmsObject>() : Arrays
				.asList(messages.value);

	}

	@Override
	public List<SmsObject> getSentMessageList(String sourcePhone,
			Integer startMessage, Integer endMessage) {
		WspPortType port = new WspPortTypeProxy();

		IntHolder code = new IntHolder();
		IntHolder resultCount = new IntHolder();
		IntHolder totalCount = new IntHolder();
		StringHolder description = new StringHolder();
		SmsArrayHolder messages = new SmsArrayHolder();

		try {
			if (port != null) {

				port.getMessageList(sourcePhone, "sent", startMessage,
						endMessage, code, description, resultCount, totalCount,
						messages);
			}

		} catch (RemoteException e) {
			logger.error("Exception when trying to get SENT messages: "
					.concat(e.getMessage()));
		}

		return (null == messages.value) ? new ArrayList<SmsObject>() : Arrays
				.asList(messages.value);
	}

	@Override
	public List<SmsObject> getDraftMessageList(String sourcePhone,
			Integer startMessage, Integer endMessage) {

		WspPortTypeProxy port = new WspPortTypeProxy();

		IntHolder code = new IntHolder();
		IntHolder resultCount = new IntHolder();
		IntHolder totalCount = new IntHolder();
		StringHolder description = new StringHolder();
		SmsArrayHolder messages = new SmsArrayHolder();

		try {
			if (port != null) {

				port.getMessageList(sourcePhone, "draft", startMessage,
						endMessage, code, description, resultCount, totalCount,
						messages);
			}

		} catch (RemoteException e) {
			logger.error("Exception when trying to get DRAFT messages: "
					.concat(e.getMessage()));
		}

		return (null == messages.value) ? new ArrayList<SmsObject>() : Arrays
				.asList(messages.value);
	}

	@Override
	public FSMSCData saveMessageAsDraft(String source, String destination,
			String text) {

		WspPortType port = new WspPortTypeProxy();

		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			if (port != null)
				port.saveDraftMessage(source, destination, text, code,
						description);
			
				
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setDestinationNumber(destination);
				data.setStatus(HttpStatus.OK.name());
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setDestinationNumber(destination);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}

		} catch (RemoteException e) {
			logger.error("Exception when trying to save draft message: "
					.concat(e.getMessage()));

		}

		return data;
	}

	@Override
	public FSMSCData sendDraftMessage(String id) {

		WspPortType port = new WspPortTypeProxy();

		Integer idInt = Integer.parseInt(id);
		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			if (port != null) {

				port.sendDraftMessage(idInt, code, description);
			}
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
				data.setMessageId(id);

				// delete message from draft folder
				port.deleteDraftMessage(idInt, code, description);
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}
		} catch (NumberFormatException e1) {
			logger.error("Error in draft message ID send as param. Please check value!");
		} catch (RemoteException e) {
			logger.error("Exception when trying to send draft message: ".concat(e.getMessage()));
		}

		return data;

	}

	@Override
	public FSMSCData deleteDraftMessage(String id) {
		
		WspPortType port = new WspPortTypeProxy();

		Integer idInt = null;
		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			idInt = Integer.parseInt(id);
			if (port != null) {

				port.deleteDraftMessage(idInt, code, description);
			}
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
				data.setMessageId(id);
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}
		} catch (NumberFormatException e1) {
			logger.error("Error in draft message ID send as param. Please check value!");
		} catch (RemoteException e) {
			logger.error("Error trying to delete draft message: ".concat(e.getMessage()));
		}

		return data;
	}

	@Override
	public FSMSCData markMessageAsRead(Integer id) {
		
		WspPortType port = new WspPortTypeProxy();

		
		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			
			if (port != null) {

				port.markMessageAs(id, 1, code, description);
			}
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
				data.setMessageId(id.toString());
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}
		} catch (NumberFormatException e1) {
			logger.error("Error in inbox message ID send as param. Please check value!");
		} catch (RemoteException e) {
			logger.error("Error trying to mark message as read:".concat(e.getMessage()));
		}
		
		return data;
		
	}

	@Override
	public FSMSCData markMessageAsUnRead(Integer id) {
		
		
		WspPortType port = new WspPortTypeProxy();
		
		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			
			if (port != null) {

				port.markMessageAs(id, 0, code, description);
			}
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
				data.setMessageId(id.toString());
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}
		} catch (NumberFormatException e1) {
			logger.error("Error in inbox message ID send as param. Please check value!");
		} catch (RemoteException e) {
			logger.error("Error trying to mark message as unread:".concat(e.getMessage()));
		}
		
		return data;
		
		
		
	}

	@Override
	public FSMSCData deleteInboxMessage(Integer id) {
		
		WspPortType port = new WspPortTypeProxy();
		
		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			
			if (port != null) {
				port.deleteMessage(id, "B", code, description);
			}
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
				data.setMessageId(id.toString());
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}
		} catch (NumberFormatException e1) {
			logger.error("Error in message ID send as param. Please check value!");
		} catch (RemoteException e) {
			logger.error("Error trying to delete Inbox message:".concat(e.getMessage()));
		}
		
		return data;
		
	}

	@Override
	public FSMSCData deleteSentMessage(Integer id) {
		
		WspPortType port = new WspPortTypeProxy();
		
		IntHolder code = new IntHolder();
		StringHolder description = new StringHolder();
		FSMSCData data = new FSMSCData();
		try {
			
			if (port != null) {
				port.deleteMessage(id, "A", code, description);
			}
			if (description.value.contains("USPJESNO")) {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.OK.name());
				data.setMessageId(id.toString());
			} else {
				data = new FSMSCData();
				data.setMessage(description.value);
				data.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
			}
		} catch (NumberFormatException e1) {
			logger.error("Error in message ID send as param. Please check value!");
		} catch (RemoteException e) {
			logger.error("Error trying to delete Sent message:".concat(e.getMessage()));
		}
		
		return data;
		
	}

}
