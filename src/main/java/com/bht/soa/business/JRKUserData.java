package com.bht.soa.business;

import java.math.BigDecimal;
import java.util.List;

import com.bht.soa.business.exception.BANotFoundException;
import com.bht.soa.business.exception.CaNotFoundException;
import com.bht.soa.business.exception.PDFNotFoundException;
import com.bht.soa.gen.jrk.bainfo.EventSource;

public interface JRKUserData {

	
	
	/**
	 * Get CA ID for user by area code and number
	 * 
	 * @param areaCode - area code {033,032,035,...}
	 * @param number
	 * @return CA ID
	 */
	public BigDecimal getUserCA(String areaCode, BigDecimal number) throws CaNotFoundException;
	
	/**
	 * Get CA ID for user by username
	 * @param username - username
	 * @return CA ID
	 */
	public BigDecimal getUserCA(String username) throws CaNotFoundException;
	
	/**
	 * Returns list of EventSources for username, based on the service class name
	 * @param username
	 * @return
	 * @throws BANotFoundException
	 */
	public List<EventSource> getEventSources(String username, String techCode, String className) throws BANotFoundException;
	
	/**
	 * Returns list of EventSources for number, based on the service class name
	 * @param username
	 * @return
	 * @throws BANotFoundException
	 */
	public List<EventSource> getEventSources(String areaCode, BigDecimal number, String techCode,  String className) throws BANotFoundException;
	
	
	/**
	 * Returns list of EventSources based on any combination of technology code and class name
	 * @param areaCode
	 * @param number
	 * @param classNames
	 * @return
	 * @throws BANotFoundException
	 */
	public List<EventSource> getEventSources(String areaCode, BigDecimal number, String[] techCodes, String[] classNames) throws BANotFoundException;

	
	
	public String getBillAuthToken(String areaCode, BigDecimal number)throws BANotFoundException, PDFNotFoundException;
	
	
}
