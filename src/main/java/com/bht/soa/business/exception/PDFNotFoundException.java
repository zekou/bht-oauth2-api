package com.bht.soa.business.exception;

public class PDFNotFoundException extends Exception{

	private static final long serialVersionUID = -8325538493116425946L;

	public PDFNotFoundException(){
		super("Couldn't find PDF URL!");
	}
	
	public PDFNotFoundException(String message){
		super(message);
	}
}