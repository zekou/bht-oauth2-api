package com.bht.soa.business.exception;

public class CaNotFoundException extends Exception{


	private static final long serialVersionUID = 7262154500231242270L;

	public CaNotFoundException(){
		super("Couldn't find CA ID for user!");
	}

	
}
