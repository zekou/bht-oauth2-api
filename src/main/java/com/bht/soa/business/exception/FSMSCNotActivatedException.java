package com.bht.soa.business.exception;

import javax.ws.rs.core.Response.Status;

public class FSMSCNotActivatedException extends WSBaseException{


	private static final long serialVersionUID = 2350552611938473960L;

	public FSMSCNotActivatedException() {
		super(Status.NOT_FOUND, "User information not found!");
		
	}
	
	

}
