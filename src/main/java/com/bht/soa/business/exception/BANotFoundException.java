package com.bht.soa.business.exception;

public class BANotFoundException extends Exception {

	
	private static final long serialVersionUID = 7262154500231242270L;

	public BANotFoundException(){
		super("Couldn't find BA for user!");
	}
	
	public BANotFoundException(String message){
		super(message);
	}
}
