package com.bht.soa.business;

import java.math.BigDecimal;
import java.util.List;

import com.bht.soa.business.exception.BANotFoundException;
import com.bht.soa.data.FSMSCData;
import com.bht.soa.gen.vbox.SmsObject;

public interface VBoxBusinessService {

	/**
	 * Checks if user has FSMSC activated for given number (works for VOIP and
	 * POTS)
	 * 
	 * @param areaCode
	 *            - area code
	 * @param number
	 *            - phone number
	 * @return
	 */
	public FSMSCData userHasFSMSCctivated(String areaCode, BigDecimal number)
			throws BANotFoundException;

	/**
	 * Sends SMS message using FSMSC service
	 * 
	 * @param sourcePhone
	 * @param destinationPhone
	 * @param text
	 * @return
	 */
	public FSMSCData sendMessage(String sourcePhone, String destinationPhone,
			String text);

	/**
	 * Returns list of SMS messages from Inbox folder
	 * 
	 * @param sourcePhone
	 * @param destinationPhone
	 * @param startMessage
	 * @param endMessage
	 * @return
	 */
	public List<SmsObject> getInboxMessageList(String sourcePhone,
			Integer startMessage, Integer endMessage);

	/**
	 * Returns list of SMS messages from Sent folder
	 * 
	 * @param sourcePhone
	 * @param startMessage
	 * @param endMessage
	 * @return
	 */
	public List<SmsObject> getSentMessageList(String sourcePhone,
			Integer startMessage, Integer endMessage);

	/**
	 * Returns list of SMS messages form Draft folder
	 * 
	 * @param sourcePhone
	 * @param startMessage
	 * @param endMessage
	 * @return
	 */
	public List<SmsObject> getDraftMessageList(String sourcePhone,
			Integer startMessage, Integer endMessage);

	
	/**
	 * Saves message in Draft folder
	 * @param source
	 * @param destination
	 * @param text
	 * @return
	 */
	public FSMSCData saveMessageAsDraft(String source, String destination, String text);
	
	
	/**
	 * Send message from Draft folder for given ID.
	 * @param id
	 * @return
	 */
	public FSMSCData sendDraftMessage(String id);
	
	/**
	 * Delete message from Draft folder for given ID.
	 * @param id
	 * @return
	 */
	public FSMSCData deleteDraftMessage(String id);
	
	
	/**
	 * Mark message from Inbox folder as read for given ID.
	 * @param id
	 * @return
	 */
	public FSMSCData markMessageAsRead(Integer id);
	
	
	/**
	 * Mark message from Inbox folder as unread for given ID.
	 * @param id
	 * @return
	 */
	public FSMSCData markMessageAsUnRead(Integer id);

	
	/**
	 * Delete message in Inbox for given ID.
	 * @param id
	 * @return
	 */
	public FSMSCData deleteInboxMessage(Integer id);
	

	/**
	 * Delete message in Sent folder for given ID.
	 * @param id
	 * @return
	 */
	public FSMSCData deleteSentMessage(Integer id);
	
	
}