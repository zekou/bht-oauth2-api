package com.bht.soa.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.bht.soa.business.exception.FSMSCNotActivatedException;
import com.bht.soa.data.FSMSCData;
import com.bht.soa.gen.vbox.SmsObject;


@Path("/fsmsc/{areacode}/{number}/")
@WebService
@Produces("application/json")
public interface FSMSCWebService {

	@WebMethod
	@GET
	@Path("/active")
	public FSMSCData userActivated(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number) throws FSMSCNotActivatedException;
	
	
	@WebMethod
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/sendSMS/{destination}")
	public FSMSCData sendSMSMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("destination") @NotNull String destination, @FormParam("text") @NotNull String text );
	
	
	@WebMethod
	@POST
	@Path("/inbox/{id}/read")
	public FSMSCData markMessageAsRead(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("id") @NotNull Integer id);
	
	@WebMethod
	@POST
	@Path("/inbox/{id}/unread")
	public FSMSCData markMessageAsUnRead(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("id") @NotNull Integer id);
	
	@WebMethod
	@DELETE
	@Path("/inbox/{id}/delete")
	public FSMSCData deleteInboxMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("id") @NotNull Integer id);
	
	@WebMethod
	@GET
	@Path("/inbox/{startmsg}/{endmsg}")
	public List<SmsObject> getInboxMessages(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("startmsg") @NotNull Integer startMsg, @PathParam("endmsg") @NotNull Integer endMsg);
	
	
	
	@WebMethod
	@DELETE
	@Path("/sent/{id}/delete")
	public FSMSCData deleteSentMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("id") @NotNull Integer id);
	
	
	@WebMethod
	@GET
	@Path("/sent/{startmsg}/{endmsg}")
	public List<SmsObject> getSentMessages(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("startmsg") @NotNull Integer startMsg, @PathParam("endmsg") @NotNull Integer endMsg);
	
		
	@WebMethod
	@GET
	@Path("/drafts")
	public List<SmsObject> getDraftMessages(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number);
	
	@WebMethod
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/drafts/send/{id}")
	public FSMSCData sendDraftMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("id") @NotNull String id);
	
	@WebMethod
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/drafts/delete/{id}")
	public FSMSCData deleteDraftMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("id") @NotNull String id);
	
	@WebMethod
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/saveSMS/{destination}")
	public FSMSCData saveSMSMessageToDrafts(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number, @PathParam("destination") @NotNull String destination, @FormParam("text") @NotNull String text);
	
	
	
		
	
		
}
