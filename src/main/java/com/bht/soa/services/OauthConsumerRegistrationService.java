package com.bht.soa.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bht.soa.data.ConsumerRegistration;


@Path("/registerConsumer")
@WebService
@Produces("application/json")
public interface OauthConsumerRegistrationService {

	@WebMethod
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/")
	public ConsumerRegistration register(@FormParam("appName") @NotNull String appName,
										 @FormParam("appDescription") @NotNull String appDescription);
		
		
}
