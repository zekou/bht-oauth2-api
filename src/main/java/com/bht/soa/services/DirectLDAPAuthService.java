package com.bht.soa.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.bht.soa.data.CustomAuthResponse;


@Path("/customauth/")
@WebService
@Produces("application/xml")
public interface DirectLDAPAuthService {

	@WebMethod
	@GET
	@Path("/direct")
	public CustomAuthResponse authenticateUser();
	
	
	
}
