package com.bht.soa.services.impl;

import javax.jws.WebMethod;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bht.soa.common.LDAPAuthHelper;
import com.bht.soa.data.BHTApiStatusCode;
import com.bht.soa.data.CustomAuthResponse;
import com.bht.soa.services.DirectLDAPAuthService;
import org.apache.commons.codec.binary.Base64;

public class DirectLDAPAuthServiceImpl implements DirectLDAPAuthService{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String BASIC_AUTH_DELIMITER = ":";
	
	private LDAPAuthHelper helper;
	
	public DirectLDAPAuthServiceImpl(final LDAPAuthHelper helper) {
		this.helper = helper;
	}
		
	@Override
	@WebMethod
	@GET
	@Path("/direct")
	public CustomAuthResponse authenticateUser() {
		
		Message message = PhaseInterceptorChain.getCurrentMessage();
		//get HTTP request
		HttpServletRequest httpRequest = (HttpServletRequest)message.get(AbstractHTTPDestination.HTTP_REQUEST);
		
		//get the HTTP Authorization header
		String basicAuth = httpRequest.getHeader("Authorization");
		logger.info("Got authorization header: " + basicAuth );
		
		if(basicAuth == null || basicAuth.isEmpty()){
			return new CustomAuthResponse(BHTApiStatusCode.ERROR, "Error - you have to provide Authorization HTTP header!");
		}else{
			String authToken = basicAuth.substring("Basic ".length());
			byte[] decodedAPI = Base64.decodeBase64(authToken);
			authToken = new String(decodedAPI);
			String[] domainAndPass = authToken.split(BASIC_AUTH_DELIMITER);//TODO
			String username = domainAndPass[0];
			String password = domainAndPass[1];
			
			return helper.authenticateUser(username, password);
		}
		
	}

	
	
}
