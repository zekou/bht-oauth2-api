package com.bht.soa.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import com.bht.soa.business.VBoxBusinessService;
import com.bht.soa.business.exception.BANotFoundException;
import com.bht.soa.business.exception.FSMSCNotActivatedException;
import com.bht.soa.business.impl.VBoxBusinessServiceImpl;
import com.bht.soa.data.FSMSCData;
import com.bht.soa.gen.vbox.SmsObject;
import com.bht.soa.services.FSMSCWebService;

@Service("fsmscService")
public class FSMSCWebServiceImpl implements FSMSCWebService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Validator validator;

	@Override
	public FSMSCData userActivated(String areacode, String number) throws FSMSCNotActivatedException {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();
		FSMSCData data;
		try {
			data = service.userHasFSMSCctivated(areacode, new BigDecimal(number));
		} catch (BANotFoundException e) {
			logger.error("BA not found for user!");
			throw new FSMSCNotActivatedException();

		}
		return data;
	}

	@Override
	public FSMSCData sendSMSMessage(String areacode, String number, String destination, String text) {

		String source = "387" + areacode.substring(1) + number;

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.sendMessage(source, destination, text);

		return data;
	}

	@Override
	public List<SmsObject> getSentMessages(String areacode, String number, Integer startMsg, Integer endMsg) {

		String source = "387" + areacode.substring(1) + number;
		VBoxBusinessService service = new VBoxBusinessServiceImpl();
		List<SmsObject> data = new ArrayList<SmsObject>();
		data = service.getSentMessageList(source, startMsg, endMsg);
		return data;

	}

	@Override
	public List<SmsObject> getDraftMessages(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number) {

		String source = "387" + areacode.substring(1) + number;
		VBoxBusinessService service = new VBoxBusinessServiceImpl();
		List<SmsObject> data = new ArrayList<SmsObject>();
		data = service.getDraftMessageList(source, 1, 100);// Hardcode range
															// here to get as
															// much as possible
		return data;
	}

	@Override
	public FSMSCData saveSMSMessageToDrafts(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("destination") @NotNull String destination, @FormParam("text") @NotNull String text) {

		String source = "387" + areacode.substring(1) + number;

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.saveMessageAsDraft(source, destination, text);

		return data;

	}

	@Override
	public FSMSCData sendDraftMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("id") @NotNull String id) {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.sendDraftMessage(id);

		return data;

	}

	@Override
	public FSMSCData deleteDraftMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("id") @NotNull String id) {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.deleteDraftMessage(id);

		return data;

	}

	@Override
	@WebMethod
	@POST
	@Path("/inbox/{id}/read")
	public FSMSCData markMessageAsRead(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("id") @NotNull Integer id) {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.markMessageAsRead(id);

		return data;

	}

	@Override
	@WebMethod
	@POST
	@Path("/inbox/{id}/unread")
	public FSMSCData markMessageAsUnRead(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("id") @NotNull Integer id) {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.markMessageAsUnRead(id);

		return data;
	}

	@Override
	@WebMethod
	@DELETE
	@Path("/inbox/{id}/delete")
	public FSMSCData deleteInboxMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("id") @NotNull Integer id) {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.deleteInboxMessage(id);

		return data;

	}

	@Override
	@WebMethod
	@DELETE
	@Path("/sent/{id}/delete")
	public FSMSCData deleteSentMessage(@PathParam("areacode") @NotNull String areacode, @PathParam("number") @NotNull String number,
			@PathParam("id") @NotNull Integer id) {

		VBoxBusinessService service = new VBoxBusinessServiceImpl();

		FSMSCData data;

		data = service.deleteSentMessage(id);

		return data;
	}

	@Override
	public List<SmsObject> getInboxMessages(String areacode, String number, Integer startMsg, Integer endMsg) {

		String source = "387" + areacode.substring(1) + number;
		VBoxBusinessService service = new VBoxBusinessServiceImpl();
		List<SmsObject> data = new ArrayList<SmsObject>();
		data = service.getInboxMessageList(source, startMsg, endMsg);
		return data;
	}

}
