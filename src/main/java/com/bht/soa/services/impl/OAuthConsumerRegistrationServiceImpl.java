package com.bht.soa.services.impl;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.cxf.rs.security.oauth2.common.Client;

import com.bht.soa.data.ConsumerRegistration;
import com.bht.soa.services.OauthConsumerRegistrationService;
import com.bht.soa.services.oauth.OAuthManager;


public class OAuthConsumerRegistrationServiceImpl implements OauthConsumerRegistrationService{

	
	private OAuthManager manager;
	
	private SecureRandom randomGenerator;
		
	
	@Override
	public ConsumerRegistration register(String appName, String appDescription) {
		
		String clientId = generateClientId(appName, appDescription);
		String clientSecret = generateClientSecret(appName, appDescription);
		
		Client newClient = new Client(clientId, clientSecret, true);
		newClient.setApplicationName(appName);
		newClient.setApplicationDescription(appDescription);
		
		manager.registerClient(newClient);
		return new ConsumerRegistration(clientId, clientSecret);
				
	}

	private String generateClientId(String appName, String appDescription) {
			
		String clientID = appName.concat("-").concat(new BigInteger(130, randomGenerator).toString(32));
		return clientID;
		
	}
	
	private String generateClientSecret(String appName, String appDescription){
			
		return new BigInteger(130, randomGenerator).toString(32);
		
	}
	
	public void setDataProvider(OAuthManager manager) {
		this.manager = manager;
	}
	
	public void setRandomGenerator(SecureRandom randomGenerator) {
		this.randomGenerator = randomGenerator;
	}

	
	
}
