package com.bht.soa.services.impl;

import org.apache.cxf.rs.security.oauth2.common.UserSubject;
import org.apache.cxf.rs.security.oauth2.grants.owner.ResourceOwnerLoginHandler;

import com.bht.soa.common.AuthHelper;
import com.bht.soa.data.BHTApiStatusCode;
import com.bht.soa.data.CustomAuthResponse;

public class BHTResourceOwnerLoginHandler implements ResourceOwnerLoginHandler {

	
	private AuthHelper authHelper;
	
	public BHTResourceOwnerLoginHandler(AuthHelper authHelper){
		this.authHelper = authHelper;
	}
		
	@Override
	public UserSubject createSubject(String username, String password) {
				
		if(username != null && password != null && authHelper != null){
			CustomAuthResponse userAuthenticated = authHelper.authenticateUser(username, password);
			if(userAuthenticated.getStatus().equals(BHTApiStatusCode.OK)){
				UserSubject us = new UserSubject(username);
				return us;
			}
		}
		
		return null;
	
	}

}