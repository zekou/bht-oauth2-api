package com.bht.soa.services.oauth;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.rs.security.oauth2.common.AccessTokenRegistration;
import org.apache.cxf.rs.security.oauth2.common.Client;
import org.apache.cxf.rs.security.oauth2.common.OAuthPermission;
import org.apache.cxf.rs.security.oauth2.common.ServerAccessToken;
import org.apache.cxf.rs.security.oauth2.common.UserSubject;
import org.apache.cxf.rs.security.oauth2.grants.code.AuthorizationCodeDataProvider;
import org.apache.cxf.rs.security.oauth2.grants.code.AuthorizationCodeRegistration;
import org.apache.cxf.rs.security.oauth2.grants.code.ServerAuthorizationCodeGrant;
import org.apache.cxf.rs.security.oauth2.provider.OAuthServiceException;
import org.apache.cxf.rs.security.oauth2.tokens.bearer.BearerAccessToken;

import com.bht.soa.services.oauth.OAuthConstants;

public class OAuthManager implements AuthorizationCodeDataProvider{


	private static final OAuthPermission SEND_VBOX_SMS_PERMISSION;
	private static final OAuthPermission CHECK_VBOX_ACTIVATION_PERMISSION;
	
	
	static{
		SEND_VBOX_SMS_PERMISSION = new OAuthPermission(
				OAuthConstants.SEND_SMS_SCOPE,
				OAuthConstants.SEND_SMS_DESCRIPTION);
		SEND_VBOX_SMS_PERMISSION.setDefault(true);
		
		CHECK_VBOX_ACTIVATION_PERMISSION = new OAuthPermission(
				OAuthConstants.CHECK_VBOX_ACTIVATION_SCOPE,
				OAuthConstants.CHECK_VBOX_ACTIVATION_DESCRIPTION);
	}
	
	private Client client;
	private ServerAuthorizationCodeGrant grant;
	private ServerAccessToken accessToken;
	
	public void registerClient(Client c){
		this.client = c;
	}
	
	public Client getClient(String clientId) throws OAuthServiceException{
		return client == null || !client.getClientId().equals(clientId) ? null : client;
	}
	
	@Override
	public ServerAuthorizationCodeGrant createCodeGrant(AuthorizationCodeRegistration reg) throws OAuthServiceException {
		grant = new ServerAuthorizationCodeGrant(client, 3600L);
		grant.setRedirectUri("");
		grant.setSubject(reg.getSubject());
		
		List<String> scope = reg.getApprovedScope().isEmpty() ? reg.getRequestedScope() : reg.getApprovedScope();
		grant.setApprovedScopes(scope);
		return grant;
	}
	
	@Override
	public ServerAuthorizationCodeGrant removeCodeGrant(String code)
			throws OAuthServiceException {
		ServerAuthorizationCodeGrant theGrant = null;
		if(grant.getCode().equals(code)){
			theGrant = grant;
			grant = null;
		}
		return theGrant;
	}
	
	@Override
	public ServerAccessToken createAccessToken(AccessTokenRegistration reg)
			throws OAuthServiceException {
		
		if(reg.getSubject() == null){
			return null;
		}
		
		ServerAccessToken token = new BearerAccessToken(reg.getClient(), 3600L);
		List<String> scope = reg.getApprovedScope().isEmpty() ? reg.getRequestedScope() : reg.getApprovedScope();
		
		token.setScopes(convertScopeToPermissions(reg.getClient(), scope));
		token.setSubject(reg.getSubject());
		token.setGrantType(reg.getGrantType());
		accessToken = token;
		return token;
	}	
	
	@Override
	public ServerAccessToken getAccessToken(String tokenId)
			throws OAuthServiceException {
		
		return accessToken == null || !accessToken.getTokenKey().equals(tokenId) ? null : accessToken;
		
	}
	
	@Override
	public void removeAccessToken(ServerAccessToken token)
			throws OAuthServiceException {
		
		accessToken = null;
		
	}
	
	@Override
	public ServerAccessToken refreshAccessToken(Client client, String refreshToken,
			List<String> scopes) throws OAuthServiceException {
		
		throw new UnsupportedOperationException();
		
	}
	
	
	@Override
	public ServerAccessToken getPreauthorizedToken(Client arg0,	List<String> arg1, UserSubject arg2, String arg3) throws OAuthServiceException {
		return null;
	}
		
	@Override
	public List<OAuthPermission> convertScopeToPermissions(Client client, List<String> scopes) {
		
		List<OAuthPermission> permList = new ArrayList<OAuthPermission>();
		
		for(String scope : scopes){
			if(scope.equals(OAuthConstants.CHECK_VBOX_ACTIVATION_SCOPE)){
				permList.add(CHECK_VBOX_ACTIVATION_PERMISSION);
			}else if(scope.equals(OAuthConstants.SEND_SMS_SCOPE)){
				permList.add(SEND_VBOX_SMS_PERMISSION);
			}
		}
		
		return permList;
	}

	

	

	

	

	

	
	
	
	
}