package com.bht.soa.services.oauth;

public final class OAuthConstants {

    // Default scope/permission associated with tokens: 
  
    public static String SEND_SMS_SCOPE = "sendSMS";
    public static String SEND_SMS_DESCRIPTION = "Send the SMS";
    
    public static String CHECK_VBOX_ACTIVATION_SCOPE = "checkActivation";
    public static String CHECK_VBOX_ACTIVATION_DESCRIPTION = "Check the VBOX activation";
   
    
    private OAuthConstants() {
        
    }
    
}
