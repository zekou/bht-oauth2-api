
package com.bht.soa.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents object that contains consumer id (in Oauth terminology known as CLIENT_ID)
 * and secret (in Oauth terminology known as CLIENT_SECRET)
 * Returned when registering third party Oauth service consumer. 
 * @see {@link OauthConsumerRegistrationService}
 */
@XmlRootElement(name = "consumer")
public class ConsumerRegistration {
    private String clientID;
    private String clientSecret;
    
    public ConsumerRegistration() {
        
    }
    
    public ConsumerRegistration(String id, String secret) {
        this.clientID = id;
        this.clientSecret = secret;
    }

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

   
}
