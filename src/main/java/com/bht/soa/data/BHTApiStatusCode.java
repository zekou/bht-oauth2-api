package com.bht.soa.data;

public enum BHTApiStatusCode {
	
	OK("OK"),
	ERROR("ERROR");
	
	private String value;
	
	private BHTApiStatusCode(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}

}
