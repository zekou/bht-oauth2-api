package com.bht.soa.data;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "bhtauth")
public class CustomAuthResponse {
	
	/*
	 * Status code for authentication response
	 */
	private BHTApiStatusCode status;
	
	/*
	 * Message for authentication response
	 */
	private String message;
	
	public CustomAuthResponse(){
		
	}
	
	public CustomAuthResponse(BHTApiStatusCode status, String message){
		this.status = status;
		this.message = message;
	}
		
	public BHTApiStatusCode getStatus() {
		return status;
	}

	public void setStatus(BHTApiStatusCode status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}