package com.bht.soa.common;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bht.soa.business.JRKUserData;
import com.bht.soa.business.VBoxBusinessService;
import com.bht.soa.data.BHTApiStatusCode;
import com.bht.soa.data.CustomAuthResponse;

/**
 * This class allows authentication
 * based on first 8 letters of ID found
 * on users's bill.
 * @author Amer Zec <amer.zec@bhtelecom.ba>
 */
public class IDOnBillAuthHelper implements AuthHelper {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private JRKUserData jrkBusinessService;
	
	
	public IDOnBillAuthHelper(JRKUserData jrkBusinessService) {
		this.jrkBusinessService = jrkBusinessService;
	}
		
	@Override
	public boolean userExists(String username) {
		//FIXME: Dummy implementation
		
		log.debug("Checking if user exists: [" + username + "]" );
		log.debug("User exists!");
		return true;
	}
	
	@Override
	public CustomAuthResponse authenticateUser(String username, String plainPassword) {
		log.debug("Authenticating user: [" + username + "]" );
		CustomAuthResponse response;
		String areaCode = username.substring(0, 3);
		BigDecimal number = new BigDecimal(username.substring(3));
		
		try{
			String billId = jrkBusinessService.getBillAuthToken(areaCode, number);
			if(plainPassword.contains(billId)){
				log.debug("User authenticated sucessfully!");
				response = new CustomAuthResponse(BHTApiStatusCode.OK, "User authenticated!");
			}else{
				log.debug("User not authenticated!");
				response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "User not authenticated!");
			}
		} catch ( Exception e){
			log.debug("User not authenticated!");
			response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "User not authenticated!");
		}
		return response;
	}

}