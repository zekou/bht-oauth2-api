package com.bht.soa.common;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bht.soa.data.BHTApiStatusCode;
import com.bht.soa.data.CustomAuthResponse;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
/**
 * This is helper class for LDAP authentication.
 * It can provide mean of authentication as OAuth backend.
 * 
 * @author Amer Zec <amer.zec@bhtelecom.ba>
 *
 */
public class LDAPAuthHelper implements AuthHelper{

	
	
	/*
	 * Properties configurable on deployment
	 */
	public static final String LDAP_HOST = "ldap.host";
	public static final String LDAP_PORT = "ldap.port";
	public static final String LDAP_BASE_DN = "ldap.base.dn";
	public static final String LDAP_PRINCIPAL = "ldap.principal";
	public static final String LDAP_PASSWORD = "ldap.password";
	private static final String ATTRIBUTE_MAIL = "ldap.attribute.mail";
	private static final String ATTRIBUTE_PASSWORD = "ldap.attribute.password";
	private static final String ATTRIBUTE_ACTIVE = "ldap.attribute.account.status";
		
		
	
	private String ldapHost;
	
	private int ldapPort;
	
	private String ldapBaseDN;
	
	private String ldapPrincipal;
	
	private String ldapPassword;
	
	private String ldapAttributePassword;
	
	private String ldapAttributeMail;
	
	private String ldapAttributeAccountStatus;
			
	private LDAPConnection connection;
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
		
	public LDAPAuthHelper(){
		super();
	}
	
	public LDAPAuthHelper(Map<String, String> configuration){
		
		if(configuration!=null && !configuration.isEmpty()){
			this.ldapHost = configuration.get(LDAP_HOST);
			this.ldapPort = Integer.parseInt(configuration.get(LDAP_PORT));
			this.ldapBaseDN = configuration.get(LDAP_BASE_DN);
			this.ldapPrincipal = configuration.get(LDAP_PRINCIPAL);
			this.ldapPassword = configuration.get(LDAP_PASSWORD);
			
			this.ldapAttributeMail = configuration.get(ATTRIBUTE_MAIL);
			this.ldapAttributePassword = configuration.get(ATTRIBUTE_PASSWORD);
			this.ldapAttributeAccountStatus = configuration.get(ATTRIBUTE_ACTIVE);
			
			//Make LDAP connection
			LDAPConnectionOptions options = new LDAPConnectionOptions();
			options.setAutoReconnect(true);
			options.setConnectTimeoutMillis(60000);
		
			
			try{
				connection = new LDAPConnection(options, ldapHost, ldapPort, ldapPrincipal, ldapPassword);
			}catch (LDAPException e){
				log.error("Error creating connection on LDAP!" + e.getExceptionMessage());
			}
			
		}else{
			log.error("Configuration map is null or empty!");
		}
	}
	
	@Override
	public boolean userExists(final String username){
		
		try {
			SearchResult searchResults;
			if(connection!=null)
				searchResults = connection.search(ldapBaseDN,SearchScope.SUB, "(uid=" + username + ")", ldapAttributeMail);
			else
				return false;
			
			if(searchResults.getEntryCount() > 0){
				log.debug("User " + username + " found on LDAP!");
				return true;
			}
		} catch (LDAPSearchException e) {
			log.error("Could not find user: " + username + " on Active Directory: " + e.getExceptionMessage());
			return false;
		}
		return false;
	}
	
	@Override
	public CustomAuthResponse authenticateUser(final String username, final String plainPassword){
		
		CustomAuthResponse response = null;
		
		try {
			SearchResult searchResults;
			if(connection!=null){
				searchResults = connection.search(ldapBaseDN,SearchScope.SUB, "(uid=" + username + ")", ldapAttributePassword, ldapAttributeAccountStatus);
				if(searchResults.getEntryCount() == 1){
					log.debug("User " + username + " found on LDAP!");
					SearchResultEntry resultEntry = searchResults.getSearchEntries().get(0);
					String encPass = resultEntry.getAttribute(ldapAttributePassword).getValue();
					encPass = encPass.replace("\r", "");//FIXME: Error in reading ZIMBRA attributes with "\r" terminator (internal bug)
					boolean active = resultEntry.getAttribute(ldapAttributeAccountStatus).getValue().equals("active") ? true : false;
					if (active){
						if( encPass != null && encPass.length() != 0){
							if(ZimbraPasswordUtil.SSHA.verifySSHA(encPass, plainPassword)){
								response = new CustomAuthResponse(BHTApiStatusCode.OK, "User exists with valid credentials!");
							}else{
								response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "Invalid username or password!");
							}
						}
						
					}else{
						response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "User exists but not active!");
					}
				}else{
					response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "Could not find user!");
				}
			}else{
				response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "Internal server error!");
			}
			
		} catch (LDAPSearchException e) {
			log.error("Could not find user: " + username + " on Active Directory: " + e.getExceptionMessage());
			response = new CustomAuthResponse(BHTApiStatusCode.ERROR, "Could not find user!");
		}
		return response;
	}
	
	
	
}
