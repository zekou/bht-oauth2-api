package com.bht.soa.common;

import com.bht.soa.data.CustomAuthResponse;

/**
 * Provides helping methods for authentication.
 * Implementors can provide means of authentication on 
 * different systems (LDAP, DBMS, WS etc).
 * @author Amer Zec <amer.zec@bhtelecom.ba>
 * 
 */
public interface AuthHelper {
	
	/**
	 * Provides verification method to check if user exists
	 * @param username - Username of user being verified
	 * @return true/false
	 */
	boolean userExists(final String username);
	
	
	/**
	 * Provides authentication method
	 * @param username
	 * @param plainPassword
	 * @return
	 */
	CustomAuthResponse authenticateUser(final String username, final String plainPassword);
	
}