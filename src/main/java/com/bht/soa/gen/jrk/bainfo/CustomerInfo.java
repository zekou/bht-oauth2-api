
package com.bht.soa.gen.jrk.bainfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activationdate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="address_id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="accountclass_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technology_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technology_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="eventsource" type="{http://jrkjppws.portal/types/}EventSource" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerInfo", propOrder = {
    "activationdate",
    "addressId",
    "accountclassCode",
    "technologyName",
    "technologyCode",
    "address",
    "name",
    "id",
    "eventsource"
})
public class CustomerInfo {

    @XmlElement(required = true, nillable = true)
    protected String activationdate;
    @XmlElement(name = "address_id", required = true, nillable = true)
    protected BigDecimal addressId;
    @XmlElement(name = "accountclass_code", required = true, nillable = true)
    protected String accountclassCode;
    @XmlElement(name = "technology_name", required = true, nillable = true)
    protected String technologyName;
    @XmlElement(name = "technology_code", required = true, nillable = true)
    protected String technologyCode;
    @XmlElement(required = true, nillable = true)
    protected String address;
    @XmlElement(required = true, nillable = true)
    protected String name;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal id;
    @XmlElement(nillable = true)
    protected List<EventSource> eventsource;

    /**
     * Gets the value of the activationdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationdate() {
        return activationdate;
    }

    /**
     * Sets the value of the activationdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationdate(String value) {
        this.activationdate = value;
    }

    /**
     * Gets the value of the addressId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAddressId() {
        return addressId;
    }

    /**
     * Sets the value of the addressId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAddressId(BigDecimal value) {
        this.addressId = value;
    }

    /**
     * Gets the value of the accountclassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountclassCode() {
        return accountclassCode;
    }

    /**
     * Sets the value of the accountclassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountclassCode(String value) {
        this.accountclassCode = value;
    }

    /**
     * Gets the value of the technologyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnologyName() {
        return technologyName;
    }

    /**
     * Sets the value of the technologyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnologyName(String value) {
        this.technologyName = value;
    }

    /**
     * Gets the value of the technologyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnologyCode() {
        return technologyCode;
    }

    /**
     * Sets the value of the technologyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnologyCode(String value) {
        this.technologyCode = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setId(BigDecimal value) {
        this.id = value;
    }

    /**
     * Gets the value of the eventsource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventsource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventsource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventSource }
     * 
     * 
     */
    public List<EventSource> getEventsource() {
        if (eventsource == null) {
            eventsource = new ArrayList<EventSource>();
        }
        return this.eventsource;
    }

}
