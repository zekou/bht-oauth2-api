
package com.bht.soa.gen.jrk.auth;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bht.soa.gen.jrk.auth package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bht.soa.gen.jrk.auth
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetsabacaXMLResponseElement }
     * 
     */
    public GetsabacaXMLResponseElement createGetsabacaXMLResponseElement() {
        return new GetsabacaXMLResponseElement();
    }

    /**
     * Create an instance of {@link GetsabacaXMLRowSetResponseElement }
     * 
     */
    public GetsabacaXMLRowSetResponseElement createGetsabacaXMLRowSetResponseElement() {
        return new GetsabacaXMLRowSetResponseElement();
    }

    /**
     * Create an instance of {@link GetsabacaXMLRowSetElement }
     * 
     */
    public GetsabacaXMLRowSetElement createGetsabacaXMLRowSetElement() {
        return new GetsabacaXMLRowSetElement();
    }

    /**
     * Create an instance of {@link GetsabacaXMLResponseElement.Result }
     * 
     */
    public GetsabacaXMLResponseElement.Result createGetsabacaXMLResponseElementResult() {
        return new GetsabacaXMLResponseElement.Result();
    }

    /**
     * Create an instance of {@link GetsabacaBeansResponseElement }
     * 
     */
    public GetsabacaBeansResponseElement createGetsabacaBeansResponseElement() {
        return new GetsabacaBeansResponseElement();
    }

    /**
     * Create an instance of {@link GetsabacaRowUser }
     * 
     */
    public GetsabacaRowUser createGetsabacaRowUser() {
        return new GetsabacaRowUser();
    }

    /**
     * Create an instance of {@link GetsabacaXMLRowSetResponseElement.Result }
     * 
     */
    public GetsabacaXMLRowSetResponseElement.Result createGetsabacaXMLRowSetResponseElementResult() {
        return new GetsabacaXMLRowSetResponseElement.Result();
    }

    /**
     * Create an instance of {@link GetsabacaBeansElement }
     * 
     */
    public GetsabacaBeansElement createGetsabacaBeansElement() {
        return new GetsabacaBeansElement();
    }

    /**
     * Create an instance of {@link GetsabacaXMLElement }
     * 
     */
    public GetsabacaXMLElement createGetsabacaXMLElement() {
        return new GetsabacaXMLElement();
    }

    /**
     * Create an instance of {@link GetsabacaRowBase }
     * 
     */
    public GetsabacaRowBase createGetsabacaRowBase() {
        return new GetsabacaRowBase();
    }

}
