
package com.bht.soa.gen.jrk.auth;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetsabacaRowUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetsabacaRowUser">
 *   &lt;complexContent>
 *     &lt;extension base="{http://cuprod_db02/PORTAL_AUTH_WS.wsdl/types/}GetsabacaRowBase">
 *       &lt;sequence>
 *         &lt;element name="bbaanaziv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ssaaaddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bbaaaddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ccaaaddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customersubtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ssaanaziv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ccaaid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="caaddressid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ssaaid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="mainlocationid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="mainlocationname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customertype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ccaanaziv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bbaaid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetsabacaRowUser", propOrder = {
    "bbaanaziv",
    "ssaaaddress",
    "bbaaaddress",
    "status",
    "statusname",
    "ccaaaddress",
    "customersubtype",
    "reason",
    "ssaanaziv",
    "ccaaid",
    "caaddressid",
    "ssaaid",
    "mainlocationid",
    "mainlocationname",
    "customertype",
    "ccaanaziv",
    "bbaaid"
})
public class GetsabacaRowUser
    extends GetsabacaRowBase
{

    @XmlElement(required = true, nillable = true)
    protected String bbaanaziv;
    @XmlElement(required = true, nillable = true)
    protected String ssaaaddress;
    @XmlElement(required = true, nillable = true)
    protected String bbaaaddress;
    @XmlElement(required = true, nillable = true)
    protected String status;
    @XmlElement(required = true, nillable = true)
    protected String statusname;
    @XmlElement(required = true, nillable = true)
    protected String ccaaaddress;
    @XmlElement(required = true, nillable = true)
    protected String customersubtype;
    @XmlElement(required = true, nillable = true)
    protected String reason;
    @XmlElement(required = true, nillable = true)
    protected String ssaanaziv;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal ccaaid;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal caaddressid;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal ssaaid;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal mainlocationid;
    @XmlElement(required = true, nillable = true)
    protected String mainlocationname;
    @XmlElement(required = true, nillable = true)
    protected String customertype;
    @XmlElement(required = true, nillable = true)
    protected String ccaanaziv;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal bbaaid;

    /**
     * Gets the value of the bbaanaziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBbaanaziv() {
        return bbaanaziv;
    }

    /**
     * Sets the value of the bbaanaziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBbaanaziv(String value) {
        this.bbaanaziv = value;
    }

    /**
     * Gets the value of the ssaaaddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsaaaddress() {
        return ssaaaddress;
    }

    /**
     * Sets the value of the ssaaaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsaaaddress(String value) {
        this.ssaaaddress = value;
    }

    /**
     * Gets the value of the bbaaaddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBbaaaddress() {
        return bbaaaddress;
    }

    /**
     * Sets the value of the bbaaaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBbaaaddress(String value) {
        this.bbaaaddress = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusname() {
        return statusname;
    }

    /**
     * Sets the value of the statusname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusname(String value) {
        this.statusname = value;
    }

    /**
     * Gets the value of the ccaaaddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcaaaddress() {
        return ccaaaddress;
    }

    /**
     * Sets the value of the ccaaaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcaaaddress(String value) {
        this.ccaaaddress = value;
    }

    /**
     * Gets the value of the customersubtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomersubtype() {
        return customersubtype;
    }

    /**
     * Sets the value of the customersubtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomersubtype(String value) {
        this.customersubtype = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the ssaanaziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsaanaziv() {
        return ssaanaziv;
    }

    /**
     * Sets the value of the ssaanaziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsaanaziv(String value) {
        this.ssaanaziv = value;
    }

    /**
     * Gets the value of the ccaaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCcaaid() {
        return ccaaid;
    }

    /**
     * Sets the value of the ccaaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCcaaid(BigDecimal value) {
        this.ccaaid = value;
    }

    /**
     * Gets the value of the caaddressid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCaaddressid() {
        return caaddressid;
    }

    /**
     * Sets the value of the caaddressid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCaaddressid(BigDecimal value) {
        this.caaddressid = value;
    }

    /**
     * Gets the value of the ssaaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSsaaid() {
        return ssaaid;
    }

    /**
     * Sets the value of the ssaaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSsaaid(BigDecimal value) {
        this.ssaaid = value;
    }

    /**
     * Gets the value of the mainlocationid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMainlocationid() {
        return mainlocationid;
    }

    /**
     * Sets the value of the mainlocationid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMainlocationid(BigDecimal value) {
        this.mainlocationid = value;
    }

    /**
     * Gets the value of the mainlocationname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainlocationname() {
        return mainlocationname;
    }

    /**
     * Sets the value of the mainlocationname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainlocationname(String value) {
        this.mainlocationname = value;
    }

    /**
     * Gets the value of the customertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomertype() {
        return customertype;
    }

    /**
     * Sets the value of the customertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomertype(String value) {
        this.customertype = value;
    }

    /**
     * Gets the value of the ccaanaziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcaanaziv() {
        return ccaanaziv;
    }

    /**
     * Sets the value of the ccaanaziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcaanaziv(String value) {
        this.ccaanaziv = value;
    }

    /**
     * Gets the value of the bbaaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBbaaid() {
        return bbaaid;
    }

    /**
     * Sets the value of the bbaaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBbaaid(BigDecimal value) {
        this.bbaaid = value;
    }

}
