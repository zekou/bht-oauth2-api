
package com.bht.soa.gen.jrk.pdfurl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="baId1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="mjes" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="sifTeh" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "baId1",
    "mjes",
    "sifTeh"
})
@XmlRootElement(name = "linkKopijaRacElement")
public class LinkKopijaRacElement {

    @XmlElement(required = true, nillable = true)
    protected BigDecimal baId1;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal mjes;
    @XmlElement(required = true, nillable = true)
    protected String sifTeh;

    /**
     * Gets the value of the baId1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBaId1() {
        return baId1;
    }

    /**
     * Sets the value of the baId1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBaId1(BigDecimal value) {
        this.baId1 = value;
    }

    /**
     * Gets the value of the mjes property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMjes() {
        return mjes;
    }

    /**
     * Sets the value of the mjes property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMjes(BigDecimal value) {
        this.mjes = value;
    }

    /**
     * Gets the value of the sifTeh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSifTeh() {
        return sifTeh;
    }

    /**
     * Sets the value of the sifTeh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSifTeh(String value) {
        this.sifTeh = value;
    }

}
