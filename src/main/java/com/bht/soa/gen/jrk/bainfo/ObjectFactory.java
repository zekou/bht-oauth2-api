
package com.bht.soa.gen.jrk.bainfo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bht.soa.gen.jrk.bainfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bht.soa.gen.jrk.bainfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCustomerBAInfoElement }
     * 
     */
    public GetCustomerBAInfoElement createGetCustomerBAInfoElement() {
        return new GetCustomerBAInfoElement();
    }

    /**
     * Create an instance of {@link GetCustomerDetailsElement }
     * 
     */
    public GetCustomerDetailsElement createGetCustomerDetailsElement() {
        return new GetCustomerDetailsElement();
    }

    /**
     * Create an instance of {@link GetCustomerDetailsResponseElement }
     * 
     */
    public GetCustomerDetailsResponseElement createGetCustomerDetailsResponseElement() {
        return new GetCustomerDetailsResponseElement();
    }

    /**
     * Create an instance of {@link CustomerDetails }
     * 
     */
    public CustomerDetails createCustomerDetails() {
        return new CustomerDetails();
    }

    /**
     * Create an instance of {@link GetCustomerBAInfoResponseElement }
     * 
     */
    public GetCustomerBAInfoResponseElement createGetCustomerBAInfoResponseElement() {
        return new GetCustomerBAInfoResponseElement();
    }

    /**
     * Create an instance of {@link CustomerBAInfo }
     * 
     */
    public CustomerBAInfo createCustomerBAInfo() {
        return new CustomerBAInfo();
    }

    /**
     * Create an instance of {@link EventSource }
     * 
     */
    public EventSource createEventSource() {
        return new EventSource();
    }

    /**
     * Create an instance of {@link CustomerInfo }
     * 
     */
    public CustomerInfo createCustomerInfo() {
        return new CustomerInfo();
    }

    /**
     * Create an instance of {@link ProductOffer }
     * 
     */
    public ProductOffer createProductOffer() {
        return new ProductOffer();
    }

    /**
     * Create an instance of {@link Technology }
     * 
     */
    public Technology createTechnology() {
        return new Technology();
    }

}
