
package com.bht.soa.gen.jrk.bainfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EventSource complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventSource">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="parent_eventsource_id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="childtype_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msisdnlb" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="bpoint" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="installation_address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="class_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="installation_address_id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="main_productoffer" type="{http://jrkjppws.portal/types/}ProductOffer" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="class_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bacustomer_id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sacustomer_id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="activationdate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apoint" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="area_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msisdnub" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventSource", propOrder = {
    "parentEventsourceId",
    "childtypeCode",
    "msisdnlb",
    "bpoint",
    "installationAddress",
    "className",
    "installationAddressId",
    "mainProductoffer",
    "id",
    "classCode",
    "bacustomerId",
    "userid",
    "sacustomerId",
    "activationdate",
    "apoint",
    "msisdn",
    "areaCode",
    "msisdnub",
    "username"
})
public class EventSource {

    @XmlElement(name = "parent_eventsource_id", required = true, nillable = true)
    protected BigDecimal parentEventsourceId;
    @XmlElement(name = "childtype_code", required = true, nillable = true)
    protected String childtypeCode;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal msisdnlb;
    @XmlElement(required = true, nillable = true)
    protected String bpoint;
    @XmlElement(name = "installation_address", required = true, nillable = true)
    protected String installationAddress;
    @XmlElement(name = "class_name", required = true, nillable = true)
    protected String className;
    @XmlElement(name = "installation_address_id", required = true, nillable = true)
    protected BigDecimal installationAddressId;
    @XmlElement(name = "main_productoffer", nillable = true)
    protected List<ProductOffer> mainProductoffer;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal id;
    @XmlElement(name = "class_code", required = true, nillable = true)
    protected String classCode;
    @XmlElement(name = "bacustomer_id", required = true, nillable = true)
    protected BigDecimal bacustomerId;
    @XmlElement(required = true, nillable = true)
    protected String userid;
    @XmlElement(name = "sacustomer_id", required = true, nillable = true)
    protected BigDecimal sacustomerId;
    @XmlElement(required = true, nillable = true)
    protected String activationdate;
    @XmlElement(required = true, nillable = true)
    protected String apoint;
    @XmlElement(required = true, nillable = true)
    protected String msisdn;
    @XmlElement(name = "area_code", required = true, nillable = true)
    protected String areaCode;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal msisdnub;
    @XmlElement(required = true, nillable = true)
    protected String username;

    /**
     * Gets the value of the parentEventsourceId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getParentEventsourceId() {
        return parentEventsourceId;
    }

    /**
     * Sets the value of the parentEventsourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setParentEventsourceId(BigDecimal value) {
        this.parentEventsourceId = value;
    }

    /**
     * Gets the value of the childtypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildtypeCode() {
        return childtypeCode;
    }

    /**
     * Sets the value of the childtypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildtypeCode(String value) {
        this.childtypeCode = value;
    }

    /**
     * Gets the value of the msisdnlb property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMsisdnlb() {
        return msisdnlb;
    }

    /**
     * Sets the value of the msisdnlb property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMsisdnlb(BigDecimal value) {
        this.msisdnlb = value;
    }

    /**
     * Gets the value of the bpoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBpoint() {
        return bpoint;
    }

    /**
     * Sets the value of the bpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBpoint(String value) {
        this.bpoint = value;
    }

    /**
     * Gets the value of the installationAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallationAddress() {
        return installationAddress;
    }

    /**
     * Sets the value of the installationAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallationAddress(String value) {
        this.installationAddress = value;
    }

    /**
     * Gets the value of the className property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassName() {
        return className;
    }

    /**
     * Sets the value of the className property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassName(String value) {
        this.className = value;
    }

    /**
     * Gets the value of the installationAddressId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInstallationAddressId() {
        return installationAddressId;
    }

    /**
     * Sets the value of the installationAddressId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInstallationAddressId(BigDecimal value) {
        this.installationAddressId = value;
    }

    /**
     * Gets the value of the mainProductoffer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mainProductoffer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMainProductoffer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductOffer }
     * 
     * 
     */
    public List<ProductOffer> getMainProductoffer() {
        if (mainProductoffer == null) {
            mainProductoffer = new ArrayList<ProductOffer>();
        }
        return this.mainProductoffer;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setId(BigDecimal value) {
        this.id = value;
    }

    /**
     * Gets the value of the classCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * Sets the value of the classCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassCode(String value) {
        this.classCode = value;
    }

    /**
     * Gets the value of the bacustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBacustomerId() {
        return bacustomerId;
    }

    /**
     * Sets the value of the bacustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBacustomerId(BigDecimal value) {
        this.bacustomerId = value;
    }

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserid() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserid(String value) {
        this.userid = value;
    }

    /**
     * Gets the value of the sacustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSacustomerId() {
        return sacustomerId;
    }

    /**
     * Sets the value of the sacustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSacustomerId(BigDecimal value) {
        this.sacustomerId = value;
    }

    /**
     * Gets the value of the activationdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationdate() {
        return activationdate;
    }

    /**
     * Sets the value of the activationdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationdate(String value) {
        this.activationdate = value;
    }

    /**
     * Gets the value of the apoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApoint() {
        return apoint;
    }

    /**
     * Sets the value of the apoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApoint(String value) {
        this.apoint = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the areaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * Sets the value of the areaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaCode(String value) {
        this.areaCode = value;
    }

    /**
     * Gets the value of the msisdnub property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMsisdnub() {
        return msisdnub;
    }

    /**
     * Sets the value of the msisdnub property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMsisdnub(BigDecimal value) {
        this.msisdnub = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

}
