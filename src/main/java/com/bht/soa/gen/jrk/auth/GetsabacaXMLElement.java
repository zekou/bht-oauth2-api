
package com.bht.soa.gen.jrk.auth;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pMsisdn" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pUsername" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pAreaCode",
    "pMsisdn",
    "pUsername"
})
@XmlRootElement(name = "getsabacaXMLElement")
public class GetsabacaXMLElement {

    @XmlElement(required = true, nillable = true)
    protected String pAreaCode;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal pMsisdn;
    @XmlElement(required = true, nillable = true)
    protected String pUsername;

    /**
     * Gets the value of the pAreaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAreaCode() {
        return pAreaCode;
    }

    /**
     * Sets the value of the pAreaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAreaCode(String value) {
        this.pAreaCode = value;
    }

    /**
     * Gets the value of the pMsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPMsisdn() {
        return pMsisdn;
    }

    /**
     * Sets the value of the pMsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPMsisdn(BigDecimal value) {
        this.pMsisdn = value;
    }

    /**
     * Gets the value of the pUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPUsername() {
        return pUsername;
    }

    /**
     * Sets the value of the pUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPUsername(String value) {
        this.pUsername = value;
    }

}
