/**
 * WspPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bht.soa.gen.vbox;

public interface WspPortType extends java.rmi.Remote {
    public void checkRegistration(java.lang.String address, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc, javax.xml.rpc.holders.StringHolder cat) throws java.rmi.RemoteException;
    public void userRegister(java.lang.String address, java.lang.String category, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void getFolderList(java.lang.String address, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc, com.bht.soa.gen.vbox.holders.StringArrayHolder folders) throws java.rmi.RemoteException;
    public void insertFolder(java.lang.String address, java.lang.String name, java.lang.String filter, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void deleteFolder(java.lang.String address, java.lang.String name, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void saveDraftMessage(java.lang.String source, java.lang.String destination, java.lang.String text, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void deleteDraftMessage(int id, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void sendDraftMessage(int id, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void sendMessage(java.lang.String source, java.lang.String destination, java.lang.String text, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void getMessageList(java.lang.String address, java.lang.String folder, int rowstart, int rowcount, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc, javax.xml.rpc.holders.IntHolder resultcount, javax.xml.rpc.holders.IntHolder totalcount, com.bht.soa.gen.vbox.holders.SmsArrayHolder messages) throws java.rmi.RemoteException;
    public void deleteMessage(int id, java.lang.String sideAorB, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
    public void markMessageAs(int id, int readstate, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException;
}
