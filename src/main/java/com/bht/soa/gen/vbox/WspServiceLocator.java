/**
 * WspServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bht.soa.gen.vbox;

public class WspServiceLocator extends org.apache.axis.client.Service implements com.bht.soa.gen.vbox.WspService {

    public WspServiceLocator() {
    }


    public WspServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WspServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for wspPort
    private java.lang.String wspPort_address = "http://10.110.49.196/svc/wsp/wspserver.php";

    public java.lang.String getwspPortAddress() {
        return wspPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wspPortWSDDServiceName = "wspPort";

    public java.lang.String getwspPortWSDDServiceName() {
        return wspPortWSDDServiceName;
    }

    public void setwspPortWSDDServiceName(java.lang.String name) {
        wspPortWSDDServiceName = name;
    }

    public com.bht.soa.gen.vbox.WspPortType getwspPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wspPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwspPort(endpoint);
    }

    public com.bht.soa.gen.vbox.WspPortType getwspPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bht.soa.gen.vbox.WspBindingStub _stub = new com.bht.soa.gen.vbox.WspBindingStub(portAddress, this);
            _stub.setPortName(getwspPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwspPortEndpointAddress(java.lang.String address) {
        wspPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bht.soa.gen.vbox.WspPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bht.soa.gen.vbox.WspBindingStub _stub = new com.bht.soa.gen.vbox.WspBindingStub(new java.net.URL(wspPort_address), this);
                _stub.setPortName(getwspPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("wspPort".equals(inputPortName)) {
            return getwspPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:wsp", "wspService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:wsp", "wspPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("wspPort".equals(portName)) {
            setwspPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
