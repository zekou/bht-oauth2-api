/**
 * WspService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bht.soa.gen.vbox;

public interface WspService extends javax.xml.rpc.Service {
    public java.lang.String getwspPortAddress();

    public com.bht.soa.gen.vbox.WspPortType getwspPort() throws javax.xml.rpc.ServiceException;

    public com.bht.soa.gen.vbox.WspPortType getwspPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
