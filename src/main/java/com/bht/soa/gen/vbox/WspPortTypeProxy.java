package com.bht.soa.gen.vbox;

public class WspPortTypeProxy implements com.bht.soa.gen.vbox.WspPortType {
  private String _endpoint = null;
  private com.bht.soa.gen.vbox.WspPortType wspPortType = null;
  
  public WspPortTypeProxy() {
    _initWspPortTypeProxy();
  }
  
  public WspPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initWspPortTypeProxy();
  }
  
  private void _initWspPortTypeProxy() {
    try {
      wspPortType = (new com.bht.soa.gen.vbox.WspServiceLocator()).getwspPort();
      if (wspPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wspPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wspPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wspPortType != null)
      ((javax.xml.rpc.Stub)wspPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bht.soa.gen.vbox.WspPortType getWspPortType() {
    if (wspPortType == null)
      _initWspPortTypeProxy();
    return wspPortType;
  }
  
  public void checkRegistration(java.lang.String address, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc, javax.xml.rpc.holders.StringHolder cat) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.checkRegistration(address, code, desc, cat);
  }
  
  public void userRegister(java.lang.String address, java.lang.String category, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.userRegister(address, category, code, desc);
  }
  
  public void getFolderList(java.lang.String address, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc, com.bht.soa.gen.vbox.holders.StringArrayHolder folders) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.getFolderList(address, code, desc, folders);
  }
  
  public void insertFolder(java.lang.String address, java.lang.String name, java.lang.String filter, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.insertFolder(address, name, filter, code, desc);
  }
  
  public void deleteFolder(java.lang.String address, java.lang.String name, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.deleteFolder(address, name, code, desc);
  }
  
  public void saveDraftMessage(java.lang.String source, java.lang.String destination, java.lang.String text, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.saveDraftMessage(source, destination, text, code, desc);
  }
  
  public void deleteDraftMessage(int id, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.deleteDraftMessage(id, code, desc);
  }
  
  public void sendDraftMessage(int id, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.sendDraftMessage(id, code, desc);
  }
  
  public void sendMessage(java.lang.String source, java.lang.String destination, java.lang.String text, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.sendMessage(source, destination, text, code, desc);
  }
  
  public void getMessageList(java.lang.String address, java.lang.String folder, int rowstart, int rowcount, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc, javax.xml.rpc.holders.IntHolder resultcount, javax.xml.rpc.holders.IntHolder totalcount, com.bht.soa.gen.vbox.holders.SmsArrayHolder messages) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.getMessageList(address, folder, rowstart, rowcount, code, desc, resultcount, totalcount, messages);
  }
  
  public void deleteMessage(int id, java.lang.String sideAorB, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.deleteMessage(id, sideAorB, code, desc);
  }
  
  public void markMessageAs(int id, int readstate, javax.xml.rpc.holders.IntHolder code, javax.xml.rpc.holders.StringHolder desc) throws java.rmi.RemoteException{
    if (wspPortType == null)
      _initWspPortTypeProxy();
    wspPortType.markMessageAs(id, readstate, code, desc);
  }
  
  
}