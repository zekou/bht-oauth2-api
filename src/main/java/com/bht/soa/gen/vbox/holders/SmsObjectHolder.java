/**
 * SmsObjectHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bht.soa.gen.vbox.holders;

public final class SmsObjectHolder implements javax.xml.rpc.holders.Holder {
    public com.bht.soa.gen.vbox.SmsObject value;

    public SmsObjectHolder() {
    }

    public SmsObjectHolder(com.bht.soa.gen.vbox.SmsObject value) {
        this.value = value;
    }

}
